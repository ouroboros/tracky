package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/gorilla/mux"

	"codeberg.org/ouroboros/tracky/internal/static"
)

func handleGetIndex(w http.ResponseWriter, r *http.Request) {
	w.Write(static.IndexHTML)
}

func handleGetCSS(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "text/css")
	w.Write(static.SiteCSS)
}

func handleUnknownRequest(w http.ResponseWriter, r *http.Request) {
	x, err := httputil.DumpRequest(r, true)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}
	fmt.Println("Dumped body")
	fmt.Printf("%s", x)
	fmt.Printf("---\n\n")
	http.NotFound(w, r)
}

func main() {
	r := mux.NewRouter()

	r.Methods("GET").Path("/").HandlerFunc(handleGetIndex)
	r.Methods("GET").Path("/site.css").HandlerFunc(handleGetCSS)

	r.PathPrefix("/").HandlerFunc(handleUnknownRequest)

	http.Handle("/", r)

	log.Println("Server starting...")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
